## Distribution nourriture : 

- distribution sur place dans la cour / sur la rue
- aussi cantine sur place des fois

- filer plus facilement de la nourriture à celleux qui en ont plus besoin 
- attendre un investissement plus important de celleux qui en ont plus les moyens

Les critères peuvent être les oppressions systémiques

### Comportement :

- limiter au maximum l'expression de symptômes d'oppessions systémiques (sexisme, validisme, racisme...)

- réduire au maximum le rapport aidant.e / aidé.e

## Fonctionnement interne

- copié collé le fonctionnement de hack liquide qu'on avait déterminer déjà bien ensemble (un noyau qui décide à l'unanimité, des possibilités libres dans le respect de ça et un contrôle démocratique a posteriori)

- on a "décidé" d'utiliser Matrix pour communiquer -> besoin de le mettre en place pour tout le monde

- a priori pas d'opposition à utiliser git après qu'on a testé pour le hackliquide

## Mise à dispo cuisine : 

- besoin d'une personne pour surveiller

## Débats : 

- préparation en amont du sujet pour intro par exemple
- animateurrice des débats selon mandat décidé collectivement
- temps à la fin des débats pour préparer les suivants (thème et mandat animation)

## Travaux

- Zef est mandatté pour le lien avec Oli
- de quel matos a-t-on besoin ?

## Ressources 

- comment s'équiper en matos de cuisine ?
    - atout si retour la journée (dit Lapin le 17/11/21)
    - Pépin aurait du rab apparemment
    - les encombrants notamment à des jours précis en banlieu comme à Versailles le jeudi soir

- quels réseaux d'aprovovisionnement utiliser ? Pépin ? Marmoulins ? Bons petits légumes ? Jean-Mi ? Un peu tout ?
    - jean-mi le 19/11/21 explique que la récup est en pause depuis 2 mois mais devrait reprendre, que c'est avant tout des récups "pour la maison" et il propose qu'on l'aide pour les récups et surtout pour créer le lien avec les éventuels fournisseurs

- est-ce qu'il faut créer une asso administrativement pour les collectes d'invendus des supermarchés ? (auquel cas on peut se baser sur la proposition de Lévrier à PNE pour une asso sans bureau ?)

- tout gratuit mais boîte à dons / prix libre (secret etc) possible

## Collaborations externe

- AL est chaud qu'on utilise la pièce en face de la cuisine

- collab avec les récups "de Jean-Mi" : Juliette est mandatée pour le lien avec lui

- essayer d'avoir un max de lien avec le HS et les autres activités du hangar

## Politique et éduc pop

- on peut écrire collectivement le texte "pourquoi" et s'en servir comme base commune

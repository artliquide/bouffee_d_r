# Pourquoi faire ?

## Direction générale

Notre horizon est la transformation radicale des rapports sociaux :
- la disparition des concepts de propriété privée, de marché et d'échange
- la destruction des oppressions systémiques
- le dépassement des systèmes de compétition inter-individuelle systématiques
- l'instauration d'une démocratie d'élaboration collective horizontale et organisée

Ce projet tend vers cet horizon. Ses modalités concrètes sont réfléchies selon cette boussole.

## Le sens des activités

### S'autonomiser collectivement

- récupérer de la nourriture à moindre coût (invendus etc), la transformer dans une cuisine à moindre coût également (squat, meubles fabriqués etc) et la distribuer (y compris à nous et sur place) encore à moindre coût (véhicules bricolés par nos soins etc) afin de réduire nos besoins d'argent et de dépendance à l'égard du capital et de l'état (pouvoir travailler moins par exemple pour lutter plus)

- intégrer les personnes au maximum dans le projet, même des inconnues (pas dans le noyau par contre) - l'autonomie sera collective et de masse ou ne sera pas

- s'entre-former à la récup/cuisine/distrib mais aussi à l'autogestion par la pratique (on est toustes des débutantes) et s'entre-aider à permettre que ce genre de choses apparaissent également ailleurs 

- mettre d'une certaine manière la cuisine à disposition d'autres collectifs (notamment AL lorsqu'il y a des concerts par exemple)

### Propagande et pouvoir d'agir

- montrer par la pratique que l'autogestion et l'autonomie du capital et de l'état c'est chouette (lieu et activité autogérés)

- débats systématiques (participation 100% facultative et orga horizontale avec les personnes qui participent ou souhaitent participer) après les ateliers cuisines et distributions pour réfléchir à pourquoi on est obligés de faire ça, au sens de nos actions etc

- réunion à la fin des ateliers pour s'organiser collectivement tout ce projet

- donner envie / espoir dans un monde sans capitalisme (pour lutte contre "il est plus facile d'imaginer la fin du monde que la fin du capitalisme)

- détournement pub/icono/discours du capitalisme autour de ça

- tout ça peut créer une dynamique et la possibilité de l'utiliser aussi collectivement pour d'autres projets par la suite

### Soutenir les luttes de notre camp

- si ça fait vivre et tenir des squats (déjà une expropriation de fait, même temporaire) c'est déjà top 

- essayer de s'intégrer au tissu militant dans le quartier et ailleurs en IDF, aller à la rencontre des habitantes du quartier et des militantes

- distribuer de la nourriture préparée directement aux piquets de grèves et occupations de lieux de production, de distribution et de consommation


